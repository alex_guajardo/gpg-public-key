GPG public key from alex.guajardo@inroute.com.mx

check this link for more details: https://www.maketecheasier.com/generate-gpg-keys-linux/

servers:
  - keys.openpgp.org
  - pgp.mit.edu
  - keyserver.ubuntu.com

To register the key in a server:

  gpg --keyserver <server-name> --send-keys <your-key-id>

the key can be found with thew command:

  gpg --list-keys

then take the last 8 hex characterz of the 'pub'

To check if the key is stored in a server use:

  gpg --keyserver <server-name> --search-keys <your-email-address>



